import os
import cv2
import numpy as np
import requests  # to get image from the web
import shutil  # to save it locally


class Image2terminal():
    folder_path = "imgs/"
    image_source = "https://image.geo.de/30143964/t/D6/v3/w1440/r1/-/mona-lisa-p-1024727412-jpg--81961-.jpg"

    def __init__(self) -> None:
        # Check whether the specified path exists or not
        isExist = os.path.exists(self.folder_path)

        if not isExist:
            # Create a new directory because it does not exist
            os.makedirs(self.folder_path)
            print("The new directory is created!")

        self.filename = self.folder_path+self.image_source.split("/")[-1]
        self.string = ""

    def download_image(self):

        # Open the url image, set stream to True, this will return the stream content.
        r = requests.get(self.image_source, stream=True)

        # Check if the image was retrieved successfully
        if r.status_code == 200:
            # Set decode_content value to True, otherwise the downloaded image file's size will be zero.
            r.raw.decode_content = True

            # Open a local file with wb ( write binary ) permission.
            with open(self.filename, 'wb') as f:
                shutil.copyfileobj(r.raw, f)

            print('Image sucessfully Downloaded: ', self.filename)
        else:
            print('Image Couldn\'t be retreived')

    def img_to_string(self, ):
        image = cv2.imread(self.filename)
        # resize
        image = cv2.resize(image, (120, 60))

        # grayscale
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        # define mapping of pixel values
        mapping = [" ", ".", "-", "+", "o", "8"]

        # calculate number of symbols
        n_symbols = 255/len(mapping)

        # reduce bit deepth
        downsampled = (gray/n_symbols).astype("int")
        # generate string
        string = ""
        for i in range(downsampled.shape[0]):
            for j in range(downsampled.shape[1]):
                string += mapping[downsampled[i][j]]
            string += "\n"
        self.string = string


i2s = Image2terminal()

i2s.download_image()
i2s.img_to_string()

print(i2s.string)
